# Author: <tpr@iki.fi>

package Bugzilla::Extension::DupeCount;
use strict;
use base qw(Bugzilla::Extension);

use constant NAME => 'DupeCount';
our $VERSION = '0.1';

sub buglist_columns {
    my ($self, $args) = @_;
    my $columns = $args->{columns};

    $columns->{'dupecount'} = {
        name => '(SELECT COUNT(*) FROM duplicates WHERE duplicates.dupe_of = bugs.bug_id GROUP BY bugs.bug_id)',
        title => 'Duplicate Count',
    };
}

__PACKAGE__->NAME;
