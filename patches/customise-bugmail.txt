diff --git a/template/en/default/email/bugmail-header.txt.tmpl b/template/en/default/email/bugmail-header.txt.tmpl
index 286c70b..85918ba 100644
--- a/template/en/default/email/bugmail-header.txt.tmpl
+++ b/template/en/default/email/bugmail-header.txt.tmpl
@@ -11,10 +11,12 @@
 [% show_new = isnew
               && (to_user.settings.bugmail_new_prefix.value == 'on') %]
 
-From: [% Param('mailfrom') %]
+From: [% changer.name %] <[% changer.login %]>
+Sender: [% Param('mailfrom') %]
 To: [% to_user.email %]
-Subject: [[% terms.Bug %] [%+ bug.id %]] [% 'New: ' IF show_new %][%+ bug.short_desc %]
+Subject: [[% bug.product %]] [[% terms.Bug %] [%+ bug.id %]] [% 'New: ' IF isnew %][%+ bug.short_desc %]
 Date: [% date %]
+Reply-To: bug-control@kde.org
 X-Bugzilla-Reason: [% reasonsheader %]
 X-Bugzilla-Type: [% bugmailtype %]
 X-Bugzilla-Watch-Reason: [% reasonswatchheader %]
